import { Target, BrowseTheWeb } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const LoginPage = {
    Name_Field: Target.the('Greeter name field').located(by.css('input[type="search"]')),
    Submit_Button: Target.the('spoof submit button').located(by.css('button[type="submit"]')),
    Search_Result: Target.the('related search result').located(by.css('td[class="sorting_1"]')),
    Message: Target.the('Greeter message').located(by.binding('yourName')),
    HomePage_searchfield: Target.the('accommodate search field').located(by.id('selectedPerson')),
};
