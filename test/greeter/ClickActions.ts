import { Click, Open, Task, Is, Wait } from 'serenity-js/lib/screenplay-protractor';
import { on } from 'cluster';
import { LoginPage } from './ui/LoginPageElement';

export const ClickTypes = ({
    click: (data) => Task.where(`#actor enters the submit button`,
        Click.on(data)
    ),
    clickWait: (data, waitele) => Task.where(`#actor enters the submit button`,
        Click.on(data),
        Wait.until(waitele, Is.clickable()),
    ),
});
