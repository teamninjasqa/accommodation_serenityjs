import { Enter, Is, Scroll, Task, Wait } from 'serenity-js/lib/screenplay-protractor';
import { LoginPage } from './ui/LoginPageElement';

export const EnterName = ({
    of: (name: string) => Task.where(`#actor enters their name as ${name}`,
        Wait.until(LoginPage.Name_Field, Is.visible()),
        Scroll.to(LoginPage.Name_Field),
        Enter.theValue(name).into(LoginPage.Name_Field),
    ),
});
