import { Scroll, See, Task, Text } from 'serenity-js/lib/screenplay-protractor';

import { equals } from '../assertions';
import { LoginPage } from './ui/LoginPageElement';

export const CheckIfGreeterMessage = ({
    reads: (expectedMessage: string) => Task.where(`#actor checks if the Greeter message reads "${expectedMessage}"`,
        Scroll.to(LoginPage.Message),
        See.if(Text.of(LoginPage.Message), equals(expectedMessage)),
    ),
});
