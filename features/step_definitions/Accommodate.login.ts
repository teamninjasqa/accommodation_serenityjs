import { Open, Click } from 'serenity-js/lib/screenplay-protractor';

import { CheckIfGreeterMessage, EnterName } from '../../test/greeter';
import { browser } from '../../node_modules/protractor';
import { ClickTypes } from '../../test/greeter/ClickActions';
import { LoginPage } from '../../test/greeter/ui/LoginPageElement';

export = function accommodationLogin() {
    accommodationLogin
    browser.waitForAngularEnabled(false);
    this.Given(/^(.*) wants to interact with AngularJS apps$/, function (actor: string) {
        return this.stage.theActorCalled(actor).attemptsTo(
            Open.browserOn('https://dev.risksafety.io/accommodate/#/home'),
        );
    });

    this.When(/^(?:he|she|they) introduces? (?:himself|herself|themselves) as (.*)$/, function (name: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            EnterName.of(name),

        );
    });

    this.Then(/^she select her name and submit it$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            // ClickTypes.click(LoginPage.search_result),
            ClickTypes.click(LoginPage.Search_Result),
            ClickTypes.clickWait(LoginPage.Submit_Button,LoginPage.HomePage_searchfield)
        );
    });
     /* this.Then(/^he should be greeted with "([^"]*)"$/, function(message: string) {
         return this.stage.theActorInTheSpotlight().attemptsTo(
             CheckIfGreeterMessage.reads(message),
         );
     }); */
};
